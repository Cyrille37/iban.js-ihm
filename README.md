# IBAN.JS IHM

Une petite interface pour la saisie d'un IBAN.
Utilise la librairie [iban.js](https://github.com/arhs/iban.js) pour la validation du format de l'IBAN.

Consultez la [page de démo](https://cyrille37.frama.io/iban.js-ihm/iban.html).


